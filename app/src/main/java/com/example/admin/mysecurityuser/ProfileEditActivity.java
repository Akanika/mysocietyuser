package com.example.admin.mysecurityuser;

/**
 * Created by KANIKA on 01/11/2017.
 */

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ProfileEditActivity extends AppCompatActivity {



    Button bt_submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        getSupportActionBar().setTitle("My Profile Edit");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        bt_submit =(Button)findViewById(R.id.btn_submit);

        bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProfileEditActivity.this, ProfileActivity.class));
                finish();

            }
        });

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(ProfileEditActivity.this, ProfileActivity.class));
        // your code.
    }



}
