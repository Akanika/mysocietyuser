package com.example.admin.mysecurityuser.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.mysecurityuser.NotifyGateCab;
import com.example.admin.mysecurityuser.NotifyGateDelivery;
import com.example.admin.mysecurityuser.NotifyGateHousehold;
import com.example.admin.mysecurityuser.NotifyGateSay;
import com.example.admin.mysecurityuser.NotifyGateVisitor;
import com.example.admin.mysecurityuser.R;
import com.example.admin.mysecurityuser.fragment.MyApartmentFragment;

public class NotifyGate_Adapter extends RecyclerView.Adapter<NotifyGate_Adapter.ViewHolder> {

    int[] images;
    String[] texts;
    Context context;

    public NotifyGate_Adapter(Context context,int[] images,String[] texts)
    {
        this.images = images;
        this.texts = texts;
        this.context = context;
    }

    @Override
    public NotifyGate_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.notify_adapter,null);
        ViewHolder mViewHolder = new ViewHolder(mView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(NotifyGate_Adapter.ViewHolder holder, final int position) {
       holder.img_notify.setImageResource(images[position]);
       holder.txt_notify.setText(texts[position]);


       holder.txt_notify.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               if(position == 0)
               {
                   Intent i = new Intent(context,NotifyGateVisitor.class);
                   i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   context.startActivity(i);
               }

               if(position == 1)
               {
                   Intent i = new Intent(context,NotifyGateCab.class);
                   i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   context.startActivity(i);
               }

              else  if(position == 2)
               {
                   Intent i = new Intent(context,NotifyGateDelivery.class);
                   i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   context.startActivity(i);
               }

               else  if(position == 3)
               {
                   Intent i = new Intent(context,NotifyGateHousehold.class);
                   i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   context.startActivity(i);
               }

               else  if(position == 4)
               {
                   Intent i = new Intent(context,NotifyGateVisitor.class);
                   i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   context.startActivity(i);
               }

               else  if(position == 5)
               {
                   Intent i = new Intent(context,NotifyGateSay.class);
                   i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   context.startActivity(i);
               }


           }
       });

    }

    @Override
    public int getItemCount() {
        return images.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_notify;
        TextView txt_notify;

        public ViewHolder(View itemView) {
            super(itemView);
            img_notify = (ImageView) itemView.findViewById(R.id.img_notify);
            txt_notify = (TextView) itemView.findViewById(R.id.txt_notify);
        }
    }
}
