package com.example.admin.mysecurityuser.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.mysecurityuser.R;

/**
 * Created by Admin on 10/5/2017.
 */

public class MySociety_Adapter extends RecyclerView.Adapter<MySociety_Adapter.ViewHolder> {

    int[] images;
    String[] texts;

    @Override
    public MySociety_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.society_adapter,null);
        ViewHolder mViewHolder = new ViewHolder(mView);
        return mViewHolder;

    }


    public MySociety_Adapter(int[] images,String[] texts)
    {
        this.images = images;
        this.texts = texts;
    }

    @Override
    public void onBindViewHolder(MySociety_Adapter.ViewHolder holder, int position) {
        holder.imageView.setImageResource(images[position]);
        holder.textView.setText(texts[position]);
    }

    @Override
    public int getItemCount() {
        return images.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.society_adapter_img);
            textView = (TextView) itemView.findViewById(R.id.society_adapter_txt);
            textView.setSelected(true);
        }
    }
}
