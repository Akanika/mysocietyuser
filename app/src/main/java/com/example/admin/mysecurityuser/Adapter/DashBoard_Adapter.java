package com.example.admin.mysecurityuser.Adapter;


import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.mysecurityuser.MySocietyActivity;
import com.example.admin.mysecurityuser.NotifyGate;
import com.example.admin.mysecurityuser.R;
import com.example.admin.mysecurityuser.fragment.MyApartmentFragment;

/**
 * Created by Admin on 10/5/2017.
 */

public class DashBoard_Adapter extends RecyclerView.Adapter<DashBoard_Adapter.ViewHolder> {

     int[] images;
     String[] texts;
    Context context;
    FragmentManager fm;
    ActionBar actionBar;
    private static final int TYPE_HEADER = 0;
   // private ScrollParallaxImageView.ParallaxStyle parallaxStyle;

    @Override
    public DashBoard_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_dashboard,null);
        ViewHolder mViewHolder = new ViewHolder(mView);


        return mViewHolder;
    }

    public DashBoard_Adapter(int[] images,String[] texts,Context context,FragmentManager fm,ActionBar actionBar)
    {
        this.images = images;
        this.texts = texts;
        this.context = context;
        this.fm = fm;
        this.actionBar = actionBar;
    }

    @Override
    public void onBindViewHolder(DashBoard_Adapter.ViewHolder holder, final int position) {
        holder.imageView.setImageResource(images[position]);
        holder.textView.setText(texts[position]);

       holder.imageView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               if(position==6)
               {
                  /* MySocietyActivity society_fragment = new MySocietyActivity(fm,actionBar);

                   Changing_Fragment(fm,society_fragment);*/

                  Intent i = new Intent(context,MySocietyActivity.class);
                   i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                  context.startActivity(i);
               }
               else if(position == 5)
               {
                   Intent i = new Intent(context,MyApartmentFragment.class);
                   i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   context.startActivity(i);
               }

               else if(position == 2)
               {
                   Intent i = new Intent(context,NotifyGate.class);
                   i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   context.startActivity(i);
               }

           }
       });




    }





    public void Changing_Fragment(FragmentManager fragmentManager, Fragment fragment) {
        // TODO

        String fragmentTagName = fragment.getClass().getName();
        System.out.println("Fragment Tag Name : " + fragmentTagName);
        FragmentManager manager = fragmentManager;
        boolean fragmentPopped = manager.popBackStackImmediate(fragmentTagName,0);

        if (!fragmentPopped
                && manager.findFragmentByTag(fragmentTagName) == null) {
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame, fragment, fragmentTagName);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(fragmentTagName);
            ft.commit();

        }

    }

    @Override
    public int getItemCount() {
        return images.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.dashboard_adapter_img);
            textView = itemView.findViewById(R.id.dashboard_adapter_txt);
            textView.setSelected(true);
        }
    }
}
