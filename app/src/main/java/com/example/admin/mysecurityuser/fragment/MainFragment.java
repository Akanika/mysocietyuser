package com.example.admin.mysecurityuser.fragment;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bartoszlipinski.recyclerviewheader2.RecyclerViewHeader;
import com.example.admin.mysecurityuser.Adapter.DashBoard_Adapter;
import com.example.admin.mysecurityuser.Adapter.ParallaxRecyclerAdapter;
import com.example.admin.mysecurityuser.R;
//import com.gjiazhe.scrollparallaximageview.ScrollParallaxImageView;


/**
 * Created by Admin on 10/5/2017.
 */

public class MainFragment extends Fragment {


    Context context;
    RecyclerView recyclerView;
    private RecyclerViewHeader recyclerHeader;
    Toolbar toolbar;
    ImageView imageView;
    private int lastTop=0;
    int images[] = {R.mipmap.visitor, R.mipmap.invite_visitor, R.mipmap.notify_gate, R.mipmap.daily_help, R.mipmap.maintenance_request, R.mipmap.my_apartment, R.mipmap.flat, R.mipmap.local_services};
    String list3[] = {"VISITOR", "INVITE VISITOR", "NOTIFY GATES", "MY DAILY HELP", "MAINTENANCE REQUESTS ", "MY APARTMENT", "MY SOCIETY", "LOCAL SERVICES"};
    FragmentManager fragmentManager;
    ActionBar actionBar;
    View vIEW;


    public MainFragment(Context context,Toolbar toolbar,ActionBar actionBar) {
        this.context = context;
        this.toolbar = toolbar;
        this.actionBar = actionBar;
    }


    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.main_frag, container, false);
        recyclerView = (RecyclerView) mView.findViewById(R.id.recycle_dashboard);
        fragmentManager = getActivity().getSupportFragmentManager();
        RecyclerView.LayoutManager layout = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(layout);
       /* LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);*/
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2,10, true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(new DashBoard_Adapter(images, list3, context, fragmentManager,actionBar));
        actionBar.setTitle("DashBoard");

        // recyclerView.setHasFixedSize(true);


        recyclerHeader = (RecyclerViewHeader) mView.findViewById(R.id.header);
        recyclerHeader.attachTo(recyclerView);

        vIEW = getActivity().getLayoutInflater().inflate(R.layout.my_header,null,false);
        imageView = (ImageView)vIEW.findViewById(R.id.imageView2);
        recyclerHeader.addView(vIEW);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                parallax(vIEW);
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                parallax(vIEW);
                super.onScrolled(recyclerView, dx, dy);
            }
        });




/*
        ParallaxRecyclerAdapter<DashBoard_Adapter> stringAdapter = new ParallaxRecyclerAdapter<>(images, list3, context, fragmentManager);
        stringAdapter.implementRecyclerAdapterMethods(new ParallaxRecyclerAdapter.RecyclerAdapterMethods() {
            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int i) {
               SimpleViewHolder.textView.setText(list3[i]);
               SimpleViewHolder.imageView.setImageResource(images[i]);

               SimpleViewHolder.imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(i==6)
                        {
                            MySocietyActivity society_fragment = new MySocietyActivity(fragmentManager);

                            Changing_Fragment(fragmentManager,society_fragment);
                        }
                        else if(i == 5)
                        {
                            MyApartmentFragment myApartmentFragment = new MyApartmentFragment(context);
                            Changing_Fragment(fragmentManager,myApartmentFragment);
                        }
                    }
                });
            }


            public void Changing_Fragment(FragmentManager fragmentManager, Fragment fragment) {
                // TODO

                String fragmentTagName = fragment.getClass().getName();
                System.out.println("Fragment Tag Name : " + fragmentTagName);
                FragmentManager manager = fragmentManager;
                boolean fragmentPopped = manager.popBackStackImmediate(fragmentTagName,0);

                if (!fragmentPopped
                        && manager.findFragmentByTag(fragmentTagName) == null) {
                    FragmentTransaction ft = manager.beginTransaction();
                    ft.replace(R.id.frame, fragment, fragmentTagName);
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    ft.addToBackStack(fragmentTagName);
                    ft.commit();

                }

            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
                return new SimpleViewHolder(getActivity().getLayoutInflater().inflate(R.layout.activity_dashboard, viewGroup, false));
            }

            @Override
            public int getItemCount() {
                return images.length;
            }
        });

        stringAdapter.setParallaxHeader(getActivity().getLayoutInflater().inflate(R.layout.my_header, recyclerView, false), recyclerView);
        stringAdapter.setOnParallaxScroll(new ParallaxRecyclerAdapter.OnParallaxScroll() {
            @Override
            public void onParallaxScroll(float percentage, float offset, View parallax) {
                //TODO: implement toolbar alpha. See README for details
            }
        });
        recyclerView.setAdapter(stringAdapter);

        return  mView;
    }

    static class SimpleViewHolder extends RecyclerView.ViewHolder {

        static ImageView imageView;
        static TextView textView;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.dashboard_adapter_img);
            textView = itemView.findViewById(R.id.dashboard_adapter_txt);
        }
    }

    public String getListString(int position) {
        return position + " - android";
    }*/
return mView;
}


     public void parallax(final View view)
     {
         final Rect r = new Rect();
         view.getLocalVisibleRect(r);

         if(lastTop != r.top)
         {
             lastTop = r.top;
             view.post(new Runnable() {
                 @Override
                 public void run() {
                    view.setY((float)(r.top/2.0));
                 }
             });
         }
     }







        /**
         * RecyclerView item decoration - give equal margin around grid item
         */
        class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

            private int spanCount;
            private int spacing;
            private boolean includeEdge;

            public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
                this.spanCount = spanCount;
                this.spacing = spacing;
                this.includeEdge = includeEdge;
            }

            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                int position = parent.getChildAdapterPosition(view); // item position
                int column = position % spanCount; // item column

                if (includeEdge) {
                    outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                    outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                    if (position < spanCount) { // top edge
                        outRect.top = spacing;
                    }
                    outRect.bottom = spacing; // item bottom
                } else {
                    outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                    outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                    if (position >= spanCount) {
                        outRect.top = spacing; // item top
                    }
                }
            }
        }}

            /**
             * Converting dp to pixel
             */
   /* private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }*/








