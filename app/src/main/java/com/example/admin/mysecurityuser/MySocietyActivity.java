package com.example.admin.mysecurityuser;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.bartoszlipinski.recyclerviewheader2.RecyclerViewHeader;
import com.example.admin.mysecurityuser.Adapter.MySociety_Adapter;

/**
 * Created by Admin on 10/5/2017.
 */

public class MySocietyActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    FragmentManager fragmentManager;
    private RecyclerViewHeader recyclerHeader;
    View vIEW;
    ImageView imageView;
    ActionBar actionBar;
    private int lastTop=0;
    int[]  images = {R.mipmap.notice_board,R.mipmap.poll_survey,R.mipmap.society_policies
            ,R.mipmap.emergency_contacts,R.mipmap.resident_directory,R.mipmap.intercom_directory,R.mipmap.mangement_commitee
           ,R.mipmap.smart_intercom};
    String[] list = {"NOTICE BOARD","POLL & SURVEY","SOCIETY POLICIES","EMERGENCY CONTACTS","RESIDENTS DIRECTORY","INTERCOM DIRECTORY","MANAGEMENT COMMITTEE","I HAVE SOMETHING TO SAY"};

    public MySocietyActivity(FragmentManager fragmentManager, ActionBar actionBar)
    {
      //this.actionBar = actionBar;
    }

    public MySocietyActivity()
    {}


    @Nullable
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //View mView = inflater.inflate(R.layout.society_frag,container,false);
        setContentView(R.layout.society_frag);
        recyclerView = (RecyclerView) findViewById(R.id.recycle_society);
        fragmentManager = getSupportFragmentManager();
        RecyclerView.LayoutManager layout = new GridLayoutManager(getApplication(),2);
        recyclerView.setLayoutManager(layout);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2,10, true));
        recyclerView.setAdapter(new MySociety_Adapter(images,list));
        actionBar = getSupportActionBar();
        actionBar.setTitle("MySociety");

        recyclerHeader = (RecyclerViewHeader) findViewById(R.id.header);
        recyclerHeader.attachTo(recyclerView);

        vIEW = getLayoutInflater().inflate(R.layout.my_header,null,false);
        imageView = (ImageView)vIEW.findViewById(R.id.imageView2);
        recyclerHeader.addView(vIEW);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                parallax(vIEW);
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                parallax(vIEW);
                super.onScrolled(recyclerView, dx, dy);
            }
        });
       // return mView;
    }


    public void parallax(final View view)
    {
        final Rect r = new Rect();
        view.getLocalVisibleRect(r);

        if(lastTop != r.top)
        {
            lastTop = r.top;
            view.post(new Runnable() {
                @Override
                public void run() {
                    view.setY((float)(r.top/2.0));
                }
            });
        }
    }





 /**
 * RecyclerView item decoration - give equal margin around grid item
 */
class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

    private int spanCount;
    private int spacing;
    private boolean includeEdge;

    public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
        this.spanCount = spanCount;
        this.spacing = spacing;
        this.includeEdge = includeEdge;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view); // item position
        int column = position % spanCount; // item column

        if (includeEdge) {
            outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
            outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

            if (position < spanCount) { // top edge
                outRect.top = spacing;
            }
            outRect.bottom = spacing; // item bottom
        } else {
            outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
            outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
            if (position >= spanCount) {
                outRect.top = spacing; // item top
            }
        }
    }
}}
