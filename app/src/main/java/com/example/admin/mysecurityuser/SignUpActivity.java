package com.example.admin.mysecurityuser;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

public class SignUpActivity extends AppCompatActivity {


    Button btn_next;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        btn_next = (Button) findViewById(R.id.btn_signup_signup);


        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SignUpActivity.this, R.style.DialogTheme);
                LayoutInflater inflater =SignUpActivity.this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.customdialog_layout, null);
                dialogBuilder.setView(dialogView);

                final AlertDialog alertDialog = dialogBuilder.create();
                Window window = alertDialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);

                Button ok_btn = (Button) dialogView.findViewById(R.id.dialog_btn);
                ok_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.hide();
                        Intent i = new Intent(SignUpActivity.this,LoginActivity.class);
                        startActivity(i);
                    }
                });

                alertDialog.show();*/

               Intent i = new Intent(SignUpActivity.this,NextSignUpActivity.class);
                startActivity(i);
            }
        });
    }
}
