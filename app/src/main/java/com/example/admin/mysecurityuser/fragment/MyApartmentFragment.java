package com.example.admin.mysecurityuser.fragment;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bartoszlipinski.recyclerviewheader2.RecyclerViewHeader;
import com.example.admin.mysecurityuser.Adapter.DashBoard_Adapter;
import com.example.admin.mysecurityuser.R;
//import com.gjiazhe.scrollparallaximageview.ScrollParallaxImageView;

/**
 * Created by Admin on 10/6/2017.
 */

public class MyApartmentFragment extends AppCompatActivity {


    RecyclerView recyclerView;
    Context context;
    FragmentManager fragmentManager;
    private RecyclerViewHeader recyclerHeader;
    View vIEW;
    ImageView imageView;
    private int lastTop=0;
    ActionBar actionBar;
    int images[] = {R.mipmap.my_family,R.mipmap.smart_intercom};
    String list3[] = {"MY FAMILY","SMART INTERCOM"};

   public  MyApartmentFragment(Context context, ActionBar actionBar)
    {
        this.context = context;
       // this.actionBar = actionBar;
    }

    public MyApartmentFragment()
    {}


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myapartment_frag);
        recyclerView = (RecyclerView) findViewById(R.id.recycle_myapartment);
        fragmentManager = getSupportFragmentManager();
        RecyclerView.LayoutManager layout = new GridLayoutManager(getApplicationContext(),2);
        recyclerView.setLayoutManager(layout);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2,10, true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(new DashBoard_Adapter(images,list3,context,fragmentManager,actionBar));
       //  actionBar.setTitle("MyApartment");
        actionBar = getSupportActionBar();
        actionBar.setTitle("MyApartment");

        recyclerHeader = (RecyclerViewHeader) findViewById(R.id.header);
        recyclerHeader.attachTo(recyclerView);

        vIEW = getLayoutInflater().inflate(R.layout.my_header,null,false);
        imageView = (ImageView)vIEW.findViewById(R.id.imageView2);
        recyclerHeader.addView(vIEW);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                parallax(vIEW);
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                parallax(vIEW);
                super.onScrolled(recyclerView, dx, dy);
            }
        });

    }




    public void parallax(final View view)
    {
        final Rect r = new Rect();
        view.getLocalVisibleRect(r);

        if(lastTop != r.top)
        {
            lastTop = r.top;
            view.post(new Runnable() {
                @Override
                public void run() {
                    view.setY((float)(r.top/2.0));
                }
            });
        }
    }


    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }}



