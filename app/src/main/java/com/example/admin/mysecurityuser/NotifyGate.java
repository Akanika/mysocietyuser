package com.example.admin.mysecurityuser;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.admin.mysecurityuser.Adapter.CustomAdapterDrawer;
import com.example.admin.mysecurityuser.Adapter.DashBoard_Adapter;
import com.example.admin.mysecurityuser.Adapter.NotifyGate_Adapter;

/**
 * Created by KANIKA on 30/10/2017.
 */

public class NotifyGate extends AppCompatActivity {

     RecyclerView recyclerView;
     ActionBar actionBar;
     int[] images = {R.mipmap.invite_visitor,R.mipmap.cab,R.mipmap.delivery_guy,R.mipmap.daily_help,
             R.mipmap.visitor,R.mipmap.feedback};
     String[] text = {"Expecting someone at my home","Cab is going to be at gate","Delivery guy will be reaching",
         "Given something to household help","Given parcel to my visitor","I have something else to say"};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notify_gate);
        recyclerView = (RecyclerView) findViewById(R.id.recycle_notify);

        LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext());
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);
        actionBar = getSupportActionBar();
        actionBar.setTitle("Notify Gate");
        recyclerView.setAdapter(new NotifyGate_Adapter(getApplicationContext(),images,text));

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }



}
