package com.example.admin.mysecurityuser.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.mysecurityuser.R;

/**
 * Created by Admin on 10/5/2017.
 */

public class CustomAdapterDrawer extends ArrayAdapter{

    Context context;
    int[] images;
    String[] titleArray;



    public CustomAdapterDrawer(Context context,String[] titles,int[] imgs) {
        super(context, R.layout.activity_listview_adapter,R.id.txt,titles);
        this.context = context;
        this.images = imgs;
        this.titleArray = titles;

    }
    class MyViewHolder
    {
        ImageView myImage;
        TextView myTitle;


        MyViewHolder(View v)
        {
            myImage = (ImageView) v.findViewById(R.id.img);
            myTitle = (TextView) v.findViewById(R.id.txt);

        }

    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View row = convertView;
        MyViewHolder holder = null;

        if(row==null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.activity_listview_adapter,parent,false);
            holder = new MyViewHolder(row);
            row.setTag(holder);
        }
        else
        {
            holder = (MyViewHolder) row.getTag();}
        holder.myImage.setImageResource(images[position]);
        holder.myTitle.setText(titleArray[position]);


        return row;
    }
}
